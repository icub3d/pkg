// Package pkg provides an example of using semantic versioning using
// tags.
package pkg

import (
	"strings"
)

// Expand puts sep in between each character in the given string.
func Expand(s string, sep string) string {
	return strings.Join(strings.Split(s, ""), sep)
}
